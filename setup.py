#!/usr/bin/env python3
# Distributed under CC0 1.0 Universal (CC0 1.0) Public Domain Dedication.
# type: ignore

import setuptools
import logging
import distutils.command.build_py
from pathlib import Path

NAME = "yukon"
DSDL_SOURCE_ROOT = Path(__file__).resolve().parent / NAME / "dsdl_src"

# noinspection PyUnresolvedReferences
class BuildPy(distutils.command.build_py.build_py):
    def run(self):
        import pyuavcan

        print("compiling", DSDL_SOURCE_ROOT)

        pyuavcan.dsdl.compile_all(
            [
                DSDL_SOURCE_ROOT / "public_regulated_data_types" / "uavcan",
                DSDL_SOURCE_ROOT / "public_unregulated_data_types" / "org_uavcan_yukon",
            ],
            output_directory=Path(".")
        )
        super().run()


logging.basicConfig(level=logging.INFO, format="%(levelname)-3.3s %(name)s: %(message)s")
setuptools.setup(
    name=NAME,
    packages=setuptools.find_packages(),
    cmdclass={"build_py": BuildPy},
)
