"""
This file contains an example PyUAVCAN node
for testing the functionality of the Yukon
UAVCAN node rendering system.
"""
import os, sys
import random
import uuid
import typing
import asyncio
import pyuavcan.application
import pyuavcan.transport.can
import pyuavcan.transport.can.media.socketcan
import org_uavcan_yukon.ui
import org_uavcan_yukon.event
import uavcan.node
import uavcan.diagnostic

class ExampleUINode:
    node_name = "org.uavcan.yukon.can_tester_node"

    def __init__(self, node_id, pub = False) -> None:
        transport, = pyuavcan.transport.can.CANTransport(
            pyuavcan.transport.can.media.socketcan.SocketCANMedia(
                "vcan0", 8
            ),
            local_node_id=node_id
        ),

        # Create node info (for uavcan.node.GetInfo)
        node_info = uavcan.node.GetInfo_1_0.Response(
            protocol_version=uavcan.node.Version_1_0(*pyuavcan.UAVCAN_SPECIFICATION_VERSION),
            software_version=uavcan.node.Version_1_0(major=1, minor=0),
            name=self.node_name,
        )

        # Create node - publishes heartbeat, receives requests
        self._node = pyuavcan.application.make_node(
            node_info,
            transport=transport
        )
        self._node.heartbeat_publisher.mode = uavcan.node.Mode_1_0.OPERATIONAL
        self._node.heartbeat_publisher.vendor_specific_status_code = os.getpid() % 100

        self._node.start()


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("node_id", type=int)
    args = parser.parse_args()

    app = ExampleUINode(args.node_id)
    app_tasks = asyncio.all_tasks(loop=asyncio.get_event_loop())

    async def list_tasks_periodically() -> None:
        """Print active tasks periodically for demo purposes."""
        while True:
            if sys.version_info >= (3, 8):
                print(
                    "\nRunning tasks:\n"
                    + "\n".join(f"{i:4}: {t.get_coro()}" for i, t in enumerate(asyncio.all_tasks())),
                    file=sys.stderr,
                )
            else:
                print(f"\nRunning {len(asyncio.all_tasks())} tasks")
            await asyncio.sleep(10)

    asyncio.get_event_loop().create_task(list_tasks_periodically())

    # The node and PyUAVCAN objects have created internal tasks, which we need to run now.
    # In this case we want to automatically stop and exit when no tasks are left to run.
    asyncio.get_event_loop().run_until_complete(asyncio.gather(*app_tasks))
