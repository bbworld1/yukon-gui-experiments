# yukon-gui-experiments: A new experimental UI testing bed for the Yukon UAVCANv1 GUI

This repository contains the types and code for a new
experimental UAVCAN-based Yukon GUI. This repository focuses
on the frontend (UI) side of things; the more backend-oriented
code can be found in (https://github.com/pavel-kirienko/yukon/).

## Design Philosophy

These goals are stated in detail in CONTRIBUTING.md (originally written
by Pavel Kirienko). The goals of the new UI are:
- To create a performant UI that can leverage modern multiprocessing environments well
  and be able to analyze high-throughput networks
- To expose an extensible API for third parties to significantly extend and alter
  the UI if desired
- To create a clean and intuitive UI, backed by an extensible and robust backend

To this end, the new Yukon GUI will run as a collection of nodes
on an internal UAVCAN network. The nodes are explained in CONTRIBUTING,
and the relevant DSDL descriptions are in yukon/dsdl_src.
- The *Head Node* is the "actual UI". It handles starting up other
  nodes and rendering the UI. Other nodes will pass *UI descriptions*
  to the Head Node, which the Head Node will then render onto a canvas
  within the GUI. The Head Node is also responsible for handling UI events,
  which will then be passed back to the node that sent the UI description via
  a *UIEvent* service call. The node will then handle the event as needed,
  possibly publishing a new UIDescription, which will cause the UI rendered on
  the canvas to change.
- The *Avatar Node* handles keeping track of the nodes on the analyzed network
  (often a physical vehicle network). It is responsible for many things (see
  CONTRIBUTING), but one important aspect of it is that it will publish
  UIDescriptions for corresponding physical nodes.
- *Canvas UI Component Nodes* are other nodes (for example, a plotter tool or
  a bus monitor) that can be rendered onto the UI cavnas. The Head Node will
  start these nodes as required. They are responsible for sending UIDescriptions
  themselves.

## Current UI State

As the name of the repo suggests, most work done here is exploratory and both
design and implementation may change at any time. Currently, there is a demo of a
UI (with Head Node) and a test UI component. To run it:
1. Build DSDL types with `python3 setup.py build` (it may take a while)
2. Run the GUI with `python3 -m yukon`
3. Run the example node with `python3 ui_node_tester.py`

If all goes well, you should see the example node rendered onto the UI canvas.
Clicking the included button should send a UIEvent to the component node, which
will then publish a new UIDescription.
