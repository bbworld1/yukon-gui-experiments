"""
This file contains an example PyUAVCAN node
for testing the functionality of the Yukon
UAVCAN node rendering system.
"""
import os, sys
import random
import uuid
import typing
import asyncio
import pyuavcan.application
import pyuavcan.transport.udp
import org_uavcan_yukon.ui
import org_uavcan_yukon.event
import uavcan.node
import uavcan.diagnostic

class ExampleUINode:
    node_name = "org.uavcan.yukon.example_ui_node"

    def __init__(self, node_id, pub = False) -> None:
        transport = pyuavcan.transport.udp.UDPTransport(
            "127.0.0.1", local_node_id=node_id
        )

        assert transport.local_node_id == node_id # Check that node ID is configured

        # Create node info (for uavcan.node.GetInfo)
        node_info = uavcan.node.GetInfo_1_0.Response(
            protocol_version=uavcan.node.Version_1_0(*pyuavcan.UAVCAN_SPECIFICATION_VERSION),
            software_version=uavcan.node.Version_1_0(major=1, minor=0),
            name=self.node_name,
        )

        # Create node - publishes heartbeat, receives requests
        self._node = pyuavcan.application.make_node(
            node_info,
            transport=transport
        )
        self._node.heartbeat_publisher.mode = uavcan.node.Mode_1_0.OPERATIONAL
        self._node.heartbeat_publisher.vendor_specific_status_code = os.getpid() % 100

        ui_event_callback = self._node.presentation.get_server(
            org_uavcan_yukon.event.UICallback_0_1, 123
        )

        ui_event_callback.serve_in_background(self._serve_ui_event_callback)

        self._pub_diagnostic_record = self._node.presentation.make_publisher_with_fixed_subject_id(
            uavcan.diagnostic.Record_1_1
        )
        self._pub_diagnostic_record.priority = pyuavcan.transport.Priority.OPTIONAL
        self._pub_diagnostic_record.send_timeout = 2.0

        self.ui_description_1 = [
            org_uavcan_yukon.ui.Button_0_1(
                text=uavcan.primitive.String_1_0(
                "I'm an example button!"
                ),
                uuid=uavcan.primitive.String_1_0(
                    str(uuid.uuid4())
                )
            ),
            org_uavcan_yukon.ui.Text_0_1(
                text=uavcan.primitive.String_1_0(
                    "And I'm some text! Hello world!"
                ),
                uuid=uavcan.primitive.String_1_0(
                    str(uuid.uuid4())
                )
            ),
            org_uavcan_yukon.ui.NodePort_0_1(
                uuid=uavcan.primitive.String_1_0(
                    str(uuid.uuid4())
                ),
                output=pub,
                title=uavcan.primitive.String_1_0(
                    "uavcan.sub.heartbeat"
                ),
                port_id=7901
            )
        ]

        self._pub_ui_desc = self._node.presentation.make_publisher(
            org_uavcan_yukon.ui.UIDescription_0_1,
            420
        )
        self._pub_ui_desc.send_timeout = 2.0

        self._node.start()

        # Publish initial UI description
        self.publish_ui_description()

    def publish_ui_description(self):
        """
        Publish a new UI description.
        This will be rendered on the canvas by
        the Yukon Head node.

        This should be called when the UI description
        (self.description) is updated and we want to
        reflect the updates on the canvas.
        """
        self._pub_ui_desc.publish_soon(
            org_uavcan_yukon.ui.UIDescription_0_1(
                title=uavcan.primitive.String_1_0(
                    "org.uavcan.yukon.TestNode1"
                ),
                elements=[
                    org_uavcan_yukon.ui.Element_0_1(
                        button=(el if isinstance(el, org_uavcan_yukon.ui.Button_0_1) else None),
                        text=(el if isinstance(el, org_uavcan_yukon.ui.Text_0_1) else None),
                        node_port=(el if isinstance(el, org_uavcan_yukon.ui.NodePort_0_1) else None),
                    )
                    for el in self.ui_description_1
                ],
            )
        )

        print("Published UI description.")

    async def _serve_ui_event_callback(
        self,
        request: org_uavcan_yukon.event.UICallback_0_1.Request,
        metadata: pyuavcan.presentation.ServiceRequestMetadata,
    ) -> typing.Optional[org_uavcan_yukon.event.UICallback_0_1.Response]:
        """
        Respond to UI event callbacks.
        """
        # Normally something would be implemented
        # here to handle the events; right now all we do is log
        # that the event was received

        # Print event and data
        print("UI Event received")
        self.ui_description_1[1] = (
            org_uavcan_yukon.ui.Text_0_1(
                text=uavcan.primitive.String_1_0(
                    f"Look, I change on click! {random.random()}"
                ),
                uuid=self.ui_description_1[1]._uuid
            )
        )
        self.publish_ui_description()

        return None


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("node_id", type=int)
    args = parser.parse_args()

    app = ExampleUINode(args.node_id)
    app_tasks = asyncio.all_tasks(loop=asyncio.get_event_loop())

    async def list_tasks_periodically() -> None:
        """Print active tasks periodically for demo purposes."""
        while True:
            if sys.version_info >= (3, 8):
                print(
                    "\nRunning tasks:\n"
                    + "\n".join(f"{i:4}: {t.get_coro()}" for i, t in enumerate(asyncio.all_tasks())),
                    file=sys.stderr,
                )
            else:
                print(f"\nRunning {len(asyncio.all_tasks())} tasks")
            await asyncio.sleep(10)

    asyncio.get_event_loop().create_task(list_tasks_periodically())

    # The node and PyUAVCAN objects have created internal tasks, which we need to run now.
    # In this case we want to automatically stop and exit when no tasks are left to run.
    asyncio.get_event_loop().run_until_complete(asyncio.gather(*app_tasks))
