"""
Various utility functions.
"""
import uavcan.primitive

import numpy as np

def array_to_str(array: np.ndarray):
    """
    Convert a uint8 array (like the kind found in uavcan.primitive.String)
    to a Python string.
    """
    return "".join(map(chr, array))
