import asyncio
from yukon.store import Store
from yukon.head_node import HeadNode
from yukon.view import YukonView

if __name__ == "__main__":
    store = Store()
    view = YukonView(store)
    app = HeadNode(store)
    app_tasks = asyncio.all_tasks(loop=asyncio.get_event_loop())

    # async def list_tasks_periodically() -> None:
    #     """Print active tasks periodically for demo purposes."""
    #     while True:
    #         if sys.version_info >= (3, 8):
    #             print(
    #                 "\nRunning tasks:\n"
    #                 + "\n".join(f"{i:4}: {t.get_coro()}" for i, t in enumerate(asyncio.all_tasks())),
    #                 file=sys.stderr,
    #             )
    #         else:
    #             print(f"\nRunning {len(asyncio.all_tasks())} tasks")
    #         await asyncio.sleep(10)

    # asyncio.get_event_loop().create_task(list_tasks_periodically())

    # The node and PyUAVCAN objects have created internal tasks, which we need to run now.
    # In this case we want to automatically stop and exit when no tasks are left to run.
    print("Running.")
    loop = asyncio.get_event_loop()
    loop.run_until_complete(asyncio.gather(*app_tasks))
