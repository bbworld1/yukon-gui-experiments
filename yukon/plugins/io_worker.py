"""
This file contains an example PyUAVCAN node
for testing the functionality of the Yukon
UAVCAN node rendering system.
"""
import os, sys
import random
import uuid
import typing
import asyncio
import pyuavcan.application
import pyuavcan.transport.can
import pyuavcan.transport.can.media.socketcan
import pyuavcan.transport.udp
import org_uavcan_yukon.ui
import org_uavcan_yukon.event
import uavcan.node
import uavcan.diagnostic

class IOWorkerNode:
    node_name = "org.uavcan.yukon.io_worker"

    def __init__(self, node_id) -> None:
        self.can_transport = pyuavcan.transport.can.CANTransport(
            pyuavcan.transport.can.media.socketcan.SocketCANMedia(
                "vcan0", 8
            ),
            local_node_id=node_id
        )

        self.udp_transport = pyuavcan.transport.udp.UDPTransport(
            "127.0.0.1", local_node_id=98
        )

        # Create node info (for uavcan.node.GetInfo)
        node_info = uavcan.node.GetInfo_1_0.Response(
            protocol_version=uavcan.node.Version_1_0(
                *pyuavcan.UAVCAN_SPECIFICATION_VERSION
            ),
            software_version=uavcan.node.Version_1_0(major=1, minor=0),
            name=self.node_name,
        )

        # Create node - publishes heartbeat, receives requests
        self._node = pyuavcan.application.make_node(
            node_info,
            transport=self.udp_transport
        )
        self._node.heartbeat_publisher.mode = uavcan.node.Mode_1_0.OPERATIONAL
        self._node.heartbeat_publisher.vendor_specific_status_code = os.getpid() % 100

        self.can_transport.begin_capture(self.handle_capture)

        self._node.start()

    def handle_capture(self, capture):
        print(capture)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("node_id", type=int)
    args = parser.parse_args()

    app = IOWorkerNode(args.node_id)
    app_tasks = asyncio.all_tasks(loop=asyncio.get_event_loop())

    print("Running.")
    # The node and PyUAVCAN objects have created internal tasks, which we need to run now.
    # In this case we want to automatically stop and exit when no tasks are left to run.
    asyncio.get_event_loop().run_until_complete(asyncio.gather(*app_tasks))
