"""
Forms the view of the GUI.
Handles rendering passed UI
descriptions and passing UI events
to the Head node.
"""
import time
import uuid
import os, sys
import threading
import typing
import asyncio

import dearpygui.dearpygui as dpg

import pyuavcan.application
import pyuavcan.transport.udp

import uavcan.primitive
import uavcan.node
import uavcan.diagnostic

import org_uavcan_yukon.ui

from yukon.util import array_to_str
from yukon.store import Store

class YukonView:
    """
    This class represents the UI view portion
    of the Yukon application. It uses Dear Imgui
    to immediately render passed UI descriptions from
    the head node, and takes care of passing UI events
    to it.
    """
    def __init__(self, store: Store) -> None:
        # Connect to the store
        self.store = store

        # Nodes
        self.nodes = {}
        self.node_ids = {}
        self.element_uuids = {}
        self.node_connections = {}
        self.node_ports = {}

        # Subscribe to callbacks
        self.store.register_ui_description_callback(self.handle_ui_description)

        # enable_docking(shift_only=False, dock_space=True)

        # Create initial node editor
        self.window = dpg.add_window(label="Yukon")
        self.tabs = dpg.add_tab_bar(parent=self.window)
        self.node_view_tab = dpg.add_tab(label="Node View", parent=self.tabs)
        self.global_register_tab = dpg.add_tab(label="Global Register View", parent=self.tabs)
        self.node_editor = dpg.add_node_editor(
            parent=self.node_view_tab,
            callback=self.link_callback,
            delink_callback=self.delink_callback
        )
        dpg.set_primary_window(self.window, True)

        # Create mainloop
        self.mainloop = threading.Thread(target=lambda: dpg.start_dearpygui())
        self.mainloop.start()

    def handle_ui_events(
        self,
        handler: int,
        app_data: typing.Any,
        user_data: typing.Tuple[int, str],
    ) -> None:
        """
        UI events callback. It takes two parameters - a sender,
        which is the id of the UI element triggering the event,
        and data, which is a tuple of the format (source_node_id, event).
        """
        source_node_id, event_type = user_data
        print("data", app_data, user_data)
        uuid = self.element_uuids[dpg.get_item_parent(handler)]
        if event_type == "click":
            self.store.call_service(
                "on_click",
                uuid=uuid,
                node_id=source_node_id,
            )

    def link_callback(self, sender, app_data):
        # app_data -> (link_id1, link_id2)
        print(sender)
        dpg.add_node_link(app_data[0], app_data[1], parent=self.node_editor)

    def delink_callback(self, sender, app_data):
        # app_data -> link_id
        dpg.delete_item(app_data)

    def handle_ui_description(
        self,
        event: str,
        ui_description: org_uavcan_yukon.ui.UIDescription_0_1,
        node_info: uavcan.node.GetInfo_1_0.Response,
        metadata: pyuavcan.transport.TransferFrom
    ) -> None:
        """
        Render a UI description passed
        from the Head node.

        This is responsible for drawing the actual
        contents of the sent UI description on the Dear
        Imgui window canvas.
        """
        node_id = metadata.source_node_id
        node_name = array_to_str(ui_description.title.value)

        if event == "remove":
            if dpg.does_item_exist(node_id):
                dpg.delete_item(node_id)
                del self.nodes[node_id]
                del self.node_ids[node_id]
                del self.node_ports[node_id]
                del self.node_connections[node_id]
        else:
            if self.node_ids.get(node_id):
                dpg.delete_item(self.node_ids[node_id], children_only=True)
                for link_id in (self.node_connections.get(node_id) or []):
                    dpg.delete_item(link_id)
            else:
                el_id = dpg.add_node(label=node_name, parent=self.node_editor)
                self.node_ids[node_id] = el_id

            self.nodes[node_id] = []
            self.node_connections[node_id] = []

            for el in ui_description.elements:
                if el.button:
                    node_attr = dpg.add_node_attribute(
                        parent=self.node_ids[node_id],
                        attribute_type=2
                    )
                    dpg_el = dpg.add_button(
                        label=array_to_str(el.button.text.value),
                        parent=node_attr
                    )
                    dpg.add_clicked_handler(
                        dpg_el,
                        callback=self.handle_ui_events,
                        user_data=(node_id, "click")
                    )
                    self.element_uuids[dpg_el] = el.button.uuid
                    self.nodes[node_id].append(node_attr)
                elif el.text:
                    node_attr = dpg.add_node_attribute(
                        parent=self.node_ids[node_id],
                        attribute_type=2
                    )
                    dpg.add_text(
                        array_to_str(el.text.text.value),
                        parent=node_attr
                    )
                    self.nodes[node_id].append(node_attr)
                elif el.node_port:
                    node_attr = dpg.add_node_attribute(
                        parent=self.node_ids[node_id],
                        attribute_type=int(el.node_port.output),
                    )
                    print(array_to_str(el.node_port.title.value))
                    dpg.add_text(
                        array_to_str(el.node_port.title.value),
                        parent=node_attr
                    )
                    self.nodes[node_id].append(node_attr)
                    if self.node_ports.get(node_id) is None:
                        self.node_ports[node_id] = {}
                    self.node_ports[node_id][el.node_port.port_id] = node_attr

            for conn in ui_description.connections:
                print("eeee")
                if self.node_ports.get(conn.other_node) is not None:
                    print("eeee2", self.node_ports, conn.other_node, conn.port_id, conn.other_port_id, self.node_ports[conn.other_node][conn.port_id], self.node_ports[conn.other_node][conn.other_port_id])
                    link_id = dpg.add_node_link(
                        self.node_ports[node_id][conn.port_id],
                        self.node_ports[conn.other_node][conn.other_port_id],
                        parent=self.node_editor
                    )
                    self.node_connections[node_id].append(link_id)


    def shutdown(self) -> None:
        """
        Shut down the UI.
        """
        pass
