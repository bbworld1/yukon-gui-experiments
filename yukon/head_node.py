"""
The Head node is responsible for interacting with the internal
Yukon UAVCAN network. It triggers rendering of the proper UI
elements.
"""
import time
import os, sys
import typing
import asyncio

import pyuavcan.application
import pyuavcan.transport.udp

import uavcan.node
import uavcan.diagnostic

import org_uavcan_yukon.ui

from yukon.store import Store
from yukon.util import array_to_str

class HeadNode:
    node_name = "org.uavcan.yukon.head_node"

    def __init__(self, store: Store) -> None:
        """
        Create and start the node with necessary subscribers and clients.
        """
        # Connect to store
        self.store = store

        transport = pyuavcan.transport.udp.UDPTransport(
            "127.0.0.1", local_node_id=68
        )

        # Create node info (for uavcan.node.GetInfo)
        node_info = uavcan.node.GetInfo_1_0.Response(
            protocol_version=uavcan.node.Version_1_0(*pyuavcan.UAVCAN_SPECIFICATION_VERSION),
            software_version=uavcan.node.Version_1_0(major=1, minor=0),
            name=self.node_name
        )

        # Create node - publishes heartbeat
        self._node = pyuavcan.application.make_node(
            node_info,
            transport=transport
        )
        self.store.register_node(self._node)
        self._node.heartbeat_publisher.mode = uavcan.node.Mode_1_0.OPERATIONAL
        self._node.heartbeat_publisher.vendor_specific_status_code = os.getpid() % 100

        # Set up diagnostic Record publisher for event logging
        self._pub_diagnostic_record = self._node.presentation.make_publisher_with_fixed_subject_id(
            uavcan.diagnostic.Record_1_1
        )
        self._pub_diagnostic_record.priority = pyuavcan.transport.Priority.OPTIONAL
        self._pub_diagnostic_record.send_timeout = 2.0

        # Set up UIDescription subscriber (so we can render UIDescriptions
        # when they get sent)
        self._sub_ui_descriptions = self._node.presentation.make_subscriber(
            org_uavcan_yukon.ui.UIDescription_0_1, 420
        )
        self._sub_ui_descriptions.receive_in_background(self._handle_ui_descriptions)

        # Set up heartbeat subscriber and timers so we know when nodes go offline
        # In the case of the demo we're removing them, but in the real
        # application it can trigger anything we want
        # Dict is of the format {node_id: last_received_time}
        self._heartbeat_timers: typing.Dict[int, int] = {}
        # self._node_ui_descriptions: typing.Dict[int, typing.List[str]] = {}
        self._sub_heartbeats = self._node.presentation.make_subscriber_with_fixed_subject_id(
            uavcan.node.Heartbeat_1_0
        )
        self._sub_heartbeats.receive_in_background(self._handle_heartbeats)

        # Create node heartbeat cleanup loop
        asyncio.ensure_future(
            self._cleanup_heartbeats(),
            loop=self._node.presentation.loop
        )

        # Register UI event handler callback
        self.store.register_service("on_click", self._on_click)

        # Start the node!
        self._node.start()

    async def _on_click(
        self,
        node_id: int,
        uuid: uavcan.primitive.String_1_0,
    ) -> None:
        """
        Handle UI event triggers from the view.
        """
        print(f"Processing UI event 'on_click' from {uuid} for {node_id}")

        client = self._node.presentation.make_client(
            org_uavcan_yukon.event.UICallback_0_1, 123, node_id
        )

        await client.call(
            org_uavcan_yukon.event.UICallback_0_1.Request(
                event=org_uavcan_yukon.event.UIEvent_0_1(
                    click_event=org_uavcan_yukon.event.Click_0_1(
                        uuid=uuid
                    )
                )
            )
        )

    async def _process_ui_events(self) -> None:
        """
        Process passed UI events in queue from
        view.
        """
        while True:
            if not self.store.ui_event_queue.empty():
                node_id, uuid, event, data = await self.store.ui_event_queue.get()

                self.store.ui_event_queue.task_done()
            else:
                # TODO: There *really* needs to be a better way to do this,
                # but otherwise this loop blocks everything.
                await asyncio.sleep(1 / 240)

    async def _handle_ui_descriptions(
        self,
        ui_description: org_uavcan_yukon.ui.UIDescription_0_1,
        metadata: pyuavcan.transport.TransferFrom
    ) -> None:
        """
        Handle rendering UI descriptions when they
        come in from nodes.
        """
        self._pub_diagnostic_record.publish_soon(
            uavcan.diagnostic.Record_1_1(
                text=f"Received UI description from {metadata.source_node_id}"
            )
        )
        print(f"Received UI description from {metadata.source_node_id}")

        node_info = await self._request_node_info(metadata.source_node_id)
        self.store.add_ui_description(ui_description, node_info, metadata)

    async def _request_node_info(self, node_id: int) -> uavcan.node.GetInfo_1_0.Response:
        """
        Utility method for requesting
        node information (e.g. name, etc.).
        """
        client = self._node.presentation.make_client_with_fixed_service_id(
            uavcan.node.GetInfo_1_0, node_id
        )

        self._pub_diagnostic_record.publish_soon(
            uavcan.diagnostic.Record_1_1(
                text=f"Requesting node info from {node_id}"
            )
        )

        response, metadata = await client.call(uavcan.node.GetInfo_1_0.Request())
        return response

    async def _handle_heartbeats(
        self,
        heartbeat: uavcan.node.Heartbeat_1_0,
        metadata: pyuavcan.transport.TransferFrom
    ) -> None:
        """
        Handle received heartbeats from other nodes
        on the network.
        """
        self._heartbeat_timers[metadata.source_node_id] = time.time()

    async def _cleanup_heartbeats(self) -> None:
        """
        Asynchronously regularly clean up heartbeats that are stale.
        """
        while True:
            now = time.time()
            cleanup = []
            for key, val in self._heartbeat_timers.items():
                if now - val > 1:
                    # Mark heartbeat for cleanup
                    cleanup.append(key)

            for node_id in cleanup:
                try:
                    self.store.remove_ui_description(node_id)
                    del self._heartbeat_timers[node_id]
                except KeyError:
                    pass

            await asyncio.sleep(0.5)
