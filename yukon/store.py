"""
Global UI state store. Stores current UI descriptions,
handles view<->head node event passing, etc.
"""
import asyncio
import typing

import pyuavcan.transport

import uavcan.node
import uavcan.primitive
import uavcan.diagnostic

import org_uavcan_yukon.ui

from yukon.util import array_to_str


class Store:
    """
    The global UI state store for Yukon.
    It holds the current UI nodes rendered,
    and handles UI events in a queue.
    """

    def __init__(self) -> None:
        # UI descriptions store
        self._ui_nodes: typing.Dict[
            str,
            typing.Tuple[
                org_uavcan_yukon.ui.UIDescription_0_1,
                uavcan.node.GetInfo_1_0.Response,
                pyuavcan.transport.TransferFrom,
            ],
        ] = {}

        # Callbacks for UI description changes
        self._ui_description_callbacks: typing.List[
            typing.Callable[
                [
                    str,
                    org_uavcan_yukon.ui.UIDescription_0_1,
                    uavcan.node.GetInfo_1_0.Response,
                    pyuavcan.transport.TransferFrom,
                ], None
            ]
        ] = []

        # Services that the view can call on the head node
        self._services = {
            "on_click": None,
            "on_node_connect": None,
            "on_node_disconnect": None,
        }

        self._node: "pyuavcan.application.Node" = None

    @property
    def ui_nodes(self):
        return self._ui_nodes.values()

    def register_node(self, node: "pyuavcan.application.Node") -> None:
        self._node = node

    def register_service(self, name: str, callback: typing.Callable) -> None:
        self._services[name] = callback

    def add_ui_description(
        self,
        ui_description: org_uavcan_yukon.ui.UIDescription_0_1,
        node_info: uavcan.node.GetInfo_1_0.Response,
        metadata: pyuavcan.transport.TransferFrom,
    ) -> None:
        """
        Add a UI description.
        """
        node_uuid = metadata.source_node_id
        self._ui_nodes[node_uuid] = (ui_description, node_info, metadata)
        for callback in self._ui_description_callbacks:
            callback("add", ui_description, node_info, metadata)

    def remove_ui_description(self, uuid: str) -> None:
        """
        Remove a UI description (e.g. on disconnect).
        """
        try:
            ui_description, node_info, metadata = self._ui_nodes[uuid]
            for callback in self._ui_description_callbacks:
                callback("remove", ui_description, node_info, metadata)

            del self._ui_nodes[uuid]
        except KeyError:
            pass

    def register_ui_description_callback(
        self,
        callback: typing.Callable[
            [
                str,
                org_uavcan_yukon.ui.UIDescription_0_1,
                uavcan.node.GetInfo_1_0.Response,
                pyuavcan.transport.TransferFrom,
            ], None
        ]
    ) -> None:
        """
        Register a description callback, which will be triggered on
        any UI description receive.
        """
        self._ui_description_callbacks.append(callback)

    def call_service(
        self,
        name: str,
        **kwargs: typing.List[typing.Any]
    ) -> typing.Any:
        asyncio.set_event_loop(self._node.presentation.loop)
        return asyncio.run_coroutine_threadsafe(
            self._services[name](**kwargs),
            self._node.presentation.loop
        )
